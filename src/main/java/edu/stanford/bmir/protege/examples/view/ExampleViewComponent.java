package edu.stanford.bmir.protege.examples.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextPane;
import org.protege.editor.core.ui.util.ComponentFactory;
import org.protege.editor.owl.ui.clsdescriptioneditor.ExpressionEditor;
import org.protege.editor.owl.ui.clsdescriptioneditor.OWLExpressionChecker;
import org.protege.editor.owl.ui.view.AbstractOWLViewComponent;
import org.semanticweb.owlapi.model.OWLClassAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExampleViewComponent extends AbstractOWLViewComponent {

    private static final Logger log = LoggerFactory.getLogger(ExampleViewComponent.class);

    private Metrics metricsComponent;
    
    private ExpressionEditor<OWLClassAxiom> owlDescriptionEditor;
    
    
   
    protected void initialiseOWLView() throws Exception {
        
        setLayout(new BorderLayout());
        metricsComponent = new Metrics(getOWLModelManager());
        add(metricsComponent);
        
        metricsComponent.setPreferredSize(new Dimension(736,376));
        
        log.info("Example View Component initialized");
    }

    /**
     *
     */
    protected void disposeOWLView() {
		metricsComponent.dispose();
	}
}
