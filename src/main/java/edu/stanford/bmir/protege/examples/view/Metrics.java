package edu.stanford.bmir.protege.examples.view;

import it.unife.ml.bundle.Bundle;
import it.unife.ml.bundle.BundleConfiguration;
import it.unife.ml.bundle.BundleConfigurationBuilder;
import it.unife.ml.probowlapi.core.ProbabilisticReasonerResult;
import it.unife.ml.probowlapi.exception.ObjectNotInitializedException;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import org.protege.editor.core.ui.util.ComponentFactory;


import org.protege.editor.owl.model.OWLModelManager;
import org.protege.editor.owl.model.event.EventType;
import org.protege.editor.owl.model.event.OWLModelManagerListener;
import org.protege.editor.owl.model.inference.OWLReasonerManager;
import org.protege.editor.owl.model.inference.ReasonerUtilities;
import org.protege.editor.owl.ui.clsdescriptioneditor.ExpressionEditor;
import org.protege.editor.owl.ui.clsdescriptioneditor.OWLExpressionChecker;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.expression.ShortFormEntityChecker;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClassAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.util.AnnotationValueShortFormProvider;
import org.semanticweb.owlapi.util.BidirectionalShortFormProvider;
import org.semanticweb.owlapi.util.BidirectionalShortFormProviderAdapter;
import org.semanticweb.owlapi.util.ShortFormProvider;
import org.semanticweb.owlapi.util.mansyntax.ManchesterOWLSyntaxParser;


public class Metrics extends JPanel {

    private JButton executeButton = new JButton("Execute");
    
    private JButton refreshButton = new JButton("Refresh");

    private JLabel textComponent = new JLabel();
    
    private JLabel n_class = new JLabel();
    
    private JTextArea textArea1 = new JTextArea();
    
    private JTextArea textArea2 = new JTextArea();
    
    private JScrollPane pane1 = new JScrollPane();
    
    private JScrollPane pane2 = new JScrollPane();
    
    private ActionListener execute ;
    
    private ActionListener refreshAction = e -> recalculate();
    
    private JSeparator separator = new JSeparator(); 
    
    private JSeparator separator1 = new JSeparator();
    
    private JSeparator separator2 = new JSeparator();
    
    private OWLModelManager modelManager;
    
    
    
    

    
    private OWLModelManagerListener modelListener = event -> {
        if (event.getType() == EventType.ACTIVE_ONTOLOGY_CHANGED) {
            recalculate();
        }
    };
    
    public Metrics(OWLModelManager modelManager) {
        
        this.modelManager = modelManager;
        recalculate();
        modelManager.addListener(modelListener);
        refreshButton.addActionListener(refreshAction);  
        
        add(textComponent);
        textComponent.setMaximumSize(new Dimension (88,16));
        textComponent.setText("Inserire Query");
        add(separator);
        separator.setPreferredSize(new Dimension (840,4));
        add(pane1, BorderLayout.PAGE_START);
        pane1.setViewportView(textArea1);
        textArea1.setPreferredSize(new Dimension(840, 160));
        textArea1.requestFocus();
        add(separator1);
        separator1.setPreferredSize(new Dimension (840,2));
        
        JPanel buttonHolder = new JPanel();
        buttonHolder.setLayout(new FlowLayout(FlowLayout.CENTER));
        buttonHolder.add(executeButton);
        executeButton.addActionListener (execute = (ActionEvent e) -> doQuery());
        buttonHolder.setMaximumSize(new Dimension(1000,50));
        add(buttonHolder);
        
        
        add(separator2);
        separator2.setPreferredSize(new Dimension (840,2));
        add(pane2, BorderLayout.PAGE_END);
        pane2.setViewportView(textArea2);
        textArea2.setPreferredSize(new Dimension(840, 160));
        textArea2.setEditable(false);
        add(n_class);
        add(refreshButton);
        
    }
    
    public void dispose() {
        modelManager.removeListener(modelListener);
        executeButton.removeActionListener(execute);
    }
    
    private void recalculate() {
        int count = modelManager.getActiveOntology().getClassesInSignature().size();
        if (count == 0) {
            count = 1;  // owl:Thing is always there.
        }
        n_class.setText("Total classes = " + count);
    }
    
    public void doQuery() 
    {
        if(isShowing()) 
        
        {
       
         
            try 
            {
            
            OWLReasonerManager reasonerManager = modelManager.getOWLReasonerManager();
            ReasonerUtilities.warnUserIfReasonerIsNotConfigured(executeButton, reasonerManager);
            
            String areaText = textArea1.getText();
        
        //load ontology
            OWLOntology ontology = modelManager.getActiveOntology();
            OWLDataFactory df = ontology.getOWLOntologyManager().getOWLDataFactory();
            
        //short form provider e bidirectional
            ShortFormProvider sfp = new AnnotationValueShortFormProvider(Arrays.asList(df.getRDFSLabel()),
                Collections.<OWLAnnotationProperty, List<String>>emptyMap(), modelManager.getOWLOntologyManager());
            BidirectionalShortFormProvider shortFormProvider =
                new BidirectionalShortFormProviderAdapter(modelManager.getOntologies(), sfp);
            ManchesterOWLSyntaxParser parser = OWLManager.createManchesterParser();
            parser.setStringToParse(areaText);
            ShortFormEntityChecker owlEntityChecker = new ShortFormEntityChecker(shortFormProvider);
            parser.setOWLEntityChecker(owlEntityChecker);
            parser.setDefaultOntology(modelManager.getActiveOntology());
        
            //parse
            OWLAxiom query = parser.parseAxiom();
            
       
        //prova bundle
           BundleConfigurationBuilder configBuilder = new BundleConfigurationBuilder(modelManager.getActiveOntology());
            BundleConfiguration config = configBuilder.buildConfiguration();
            Bundle reasoner = new Bundle(config);
            ProbabilisticReasonerResult result = reasoner.computeQuery(query);
            //initialization
            reasoner.init();
            
            textArea2.setText(result.toString());
            
           reasoner.dispose();
        }catch (OWLException ex) {
                Logger.getLogger(Metrics.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ObjectNotInitializedException ex) {
                Logger.getLogger(Metrics.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
}


